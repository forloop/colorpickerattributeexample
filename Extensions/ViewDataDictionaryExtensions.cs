using System;
using System.Linq;
using System.Web.Mvc;

namespace ColorPickerAttributeExample.Extensions
{
    public static class ViewDataDictionaryExtensions
    {
        public static TAttribute GetModelAttribute<TAttribute>(this ViewDataDictionary viewData, bool inherit = false)
            where TAttribute : Attribute
        {
            if (viewData == null) throw new ArgumentNullException("viewData");

            Type containerType = viewData.ModelMetadata.ContainerType;

            return ((TAttribute[]) containerType.GetProperty(viewData.ModelMetadata.PropertyName)
                                       .GetCustomAttributes(typeof (TAttribute), inherit)).FirstOrDefault();
        }
    }
}