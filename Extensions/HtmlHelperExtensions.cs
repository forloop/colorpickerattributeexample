﻿using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace ColorPickerAttributeExample.Extensions
{
    public static class HtmlHelperExtensions
    {
        public static IHtmlString RenderScripts(this HtmlHelper htmlHelper)
        {
            var scripts = htmlHelper.ViewContext.HttpContext.Items["Scripts"] as IList<string>;

            if (scripts != null)
            {
                var builder = new StringBuilder();

                builder.AppendLine("<script type='text/javascript'>");
                builder.AppendLine("$(function() {");
                foreach (string script in scripts)
                {
                    builder.AppendLine(script);
                }
                builder.AppendLine("});");
                builder.AppendLine("</script>");

                return new MvcHtmlString(builder.ToString());
            }
            return null;
        }
    }
}