using System;
using System.Collections.Generic;
using System.Globalization;
using System.Web;
using System.Web.Mvc;

namespace ColorPickerAttributeExample.Attributes
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public sealed class ColorPickerAttribute : Attribute, IMetadataAware
    {
        private const string Template =
            "$('#{0}').ColorPicker({{onSubmit: function(hsb, hex, rgb, el) {{" +
            "var self = $(el); self.val(hex);self.ColorPickerHide();}}, onBeforeShow: function () " +
            "{{$(this).ColorPickerSetColor(this.value);}}}}).bind('keyup', function(){{ $(this).ColorPickerSetColor(this.value); }});";

        public const string ColorPicker = "_ColorPicker";

        private int _count;

        // if using IoC container, you could inject this into the attribute
        internal HttpContextBase Context
        {
            get { return new HttpContextWrapper(HttpContext.Current); }
        }

        public string Id
        {
            get { return "jquery-colorpicker-" + _count; }
        }

        #region IMetadataAware Members

        public void OnMetadataCreated(ModelMetadata metadata)
        {
            IList<string> list = Context.Items["Scripts"] as IList<string> ?? new List<string>();
            _count = list.Count;

            metadata.TemplateHint = ColorPicker;
            metadata.AdditionalValues[ColorPicker] = Id;

            list.Add(string.Format(CultureInfo.InvariantCulture, Template, Id));

            Context.Items["Scripts"] = list;
        }

        #endregion
    }
}