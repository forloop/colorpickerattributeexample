﻿using System.Web.Mvc;
using ColorPickerAttributeExample.Models;

namespace ColorPickerAttributeExample.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Message = "Welcome to ASP.NET MVC!";

            return View(new HomeModel());
        }
    }
}