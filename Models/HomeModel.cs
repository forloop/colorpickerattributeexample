using ColorPickerAttributeExample.Attributes;

namespace ColorPickerAttributeExample.Models
{
    public class HomeModel
    {
        [ColorPicker]
        public string ColorPicker { get; set; }
    }
}